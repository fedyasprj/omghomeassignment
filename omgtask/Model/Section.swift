//
//  Section.swift
//  omgtask
//
//  Created by Fed on 21.03.2024.
//

import Foundation

struct Section {
    let id: UUID
    let number: Int
    var items: [Item]
    
    init(id: UUID = .init(), number: Int, items: [Item]) {
        self.id = id
        self.number = number
        self.items = items
    }
}

extension Section: Identifiable, Hashable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension Array where Element == Section {
    mutating func generateRandomSections(
        minSections: Int = 100,
        maxSections: Int = 200,
        minItems: Int = 10,
        maxItems: Int = 50
    ) {
        let numberOfSections = Int.random(in: minSections...maxSections)
        let sections =  (1...numberOfSections)
            .map { sectionNumber in
                let numberOfItems = Int.random(in: minItems...maxItems)
                let items = (1...numberOfItems).map { Item(number: $0) }
                return Section(number: sectionNumber, items: items)
            }
        
        self = sections
    }
    
    mutating func modifyRandomItemInEachSection() {
        for index in self.indices {
            guard !self[index].items.isEmpty else { continue }
            if let randomItemIndex = self[index].items.indices.randomElement() {
                let newRandomNumber = Int.random(in: 1...100)
                let newItem = Item(number: newRandomNumber)
                self[index].items[randomItemIndex] = newItem
            }
        }
    }
}
