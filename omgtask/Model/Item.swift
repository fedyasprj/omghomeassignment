//
//  Item.swift
//  omgtask
//
//  Created by Fed on 20.03.2024.
//

import Foundation

struct Item {
    let id: UUID
    let number: Int
    
    init(id: UUID = .init(), number: Int) {
        self.id = id
        self.number = number
    }
}

extension Item: Hashable, Identifiable, Equatable  {
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
