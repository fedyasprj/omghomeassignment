//
//  ViewController.swift
//  omgtask
//
//  Created by Fed on 20.03.2024.
//

import Combine
import UIKit

final class MainViewController: BaseViewController {
    // MARK: - Properties
    private let dataService = DataService()
    
    private var collectionView: UICollectionView!
    private var dataSource: DataSource?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        configureDataSource()
        setupDataListener()
        dataService.startGettingData()
    }
    
    // MARK: - Setup Methods
    private func setupDataListener() {
        dataService.$sections
            .dropFirst()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] sections in
                guard let self else { return }
                applySnapshot(for: sections)
            }
            .store(in: &subscriptionsSet)
    }
    
    private func configureCollectionView() {
        collectionView = SquareCollectionView(frame: view.bounds)
        view.addSubview(collectionView)
    }
    
    private func configureDataSource() {
        dataSource = DataSource(
            collectionView: collectionView,
            cellProvider: { collectionView, indexPath, item in
                guard let cell = collectionView
                    .dequeueReusableCell(withReuseIdentifier: SquareCell.identifier, for: indexPath) as? SquareCell else {
                    fatalError("Unable to dequeue cell")
                }
                cell.configure(with: item)
                return cell
            }
        )
    }
    
    private func applySnapshot(for sections: [Section]) {
        var snapshot = Snapshot()
        snapshot.appendSections(sections)
        sections.forEach {
            snapshot.appendItems($0.items, toSection: $0)
        }
        dataSource?.apply(snapshot, animatingDifferences: true)
    }
}
