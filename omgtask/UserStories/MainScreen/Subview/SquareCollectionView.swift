//
//  SquareCollectionView.swift
//  omgtask
//
//  Created by Fed on 22.03.2024.
//

import UIKit

final class SquareCollectionView: UICollectionView {
    // MARK: - Initializer
    init(frame: CGRect) {
        let layout = UICollectionViewCompositionalLayout { _, _ -> NSCollectionLayoutSection? in
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                  heightDimension: .fractionalHeight(1))
            let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
            layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 8)
            
            let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .estimated(104),
                                                         heightDimension: .estimated(88))
            let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])
            
            let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
            layoutSection.orthogonalScrollingBehavior = .continuous
            layoutSection.contentInsets = NSDirectionalEdgeInsets(top: 16, leading: 12, bottom: 0, trailing: 12)
            return layoutSection
        }
        super.init(frame: frame, collectionViewLayout: layout)
        register(SquareCell.self, forCellWithReuseIdentifier: SquareCell.identifier)
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:))))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    @objc private func handleLongPress(gesture: UILongPressGestureRecognizer) {
        guard
            let targetIndexPath = self.indexPathForItem(at: gesture.location(in: self)),
            let cell = self.cellForItem(at: targetIndexPath) else {
                return
        }
        
        switch gesture.state {
        case .began:
            UIView.animate(withDuration: 0.2) {
                cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            }
        case .ended, .cancelled, .failed, .changed:
            UIView.animate(withDuration: 0.2) {
                cell.transform = .identity
            }
        default:
            break
        }
    }
}
