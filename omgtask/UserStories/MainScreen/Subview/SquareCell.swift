//
//  SquareCell.swift
//  omgtask
//
//  Created by Fed on 20.03.2024.
//

import UIKit

final class SquareCell: UICollectionViewCell {
    // MARK: - Properties
    private let textLabel = UILabel()
    
    // MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup Methods
    func configure(with model: Item) {
        textLabel.text = "\(model.number)"
    }
    
    private func setupUI() {
        contentView.backgroundColor = .systemGreen
        layer.cornerRadius = 10
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
    }
    
    private func setupConstraints() {
        contentView.addSubview(textLabel)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            textLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            textLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}

extension SquareCell: ReusableCellProtocol { }
