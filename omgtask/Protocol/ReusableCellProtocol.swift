//
//  ReusableCellProtocol.swift
//  omgtask
//
//  Created by Fed on 20.03.2024.
//

import Foundation

protocol ReusableCellProtocol {
    static var identifier: String { get }
}

extension ReusableCellProtocol {
    static var identifier: String {
        String(describing: self)
    }
}
