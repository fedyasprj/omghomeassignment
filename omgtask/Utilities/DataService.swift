//
//  DataService.swift
//  omgtask
//
//  Created by Fed on 20.03.2024.
//

import Combine
import Foundation

final class DataService: ObservableObject {
    @Published var sections: [Section] = []
    
    func startGettingData() {
        sections.generateRandomSections()
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            guard let self else { return }
            sections.modifyRandomItemInEachSection()
            objectWillChange.send()
        }
    }
}
