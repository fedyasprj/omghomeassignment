//
//  AppTypealias.swift
//  omgtask
//
//  Created by Fed on 20.03.2024.
//

import UIKit

typealias DataSource =  UICollectionViewDiffableDataSource<Section, Item>
typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Item>
