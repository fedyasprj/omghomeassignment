//
//  BaseViewController.swift
//  omgtask
//
//  Created by Fed on 20.03.2024.
//

import Combine
import UIKit

class BaseViewController: UIViewController {
    lazy var subscriptionsSet = Set<AnyCancellable>()
}
